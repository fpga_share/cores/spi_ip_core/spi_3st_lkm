/*****************************************************************************
 * Описание:
 *      Топ-файл драйвера ip ядра spi_3st
 *
 * Комментарии:
 *
 * Разработчик:
 *      Igor Bolshevikov <bolshevikov.igor@gmail.com>
 *
 * license:
 *      This program is free software; you can redistribute it and/or modify it
 *      under the terms of the GNU General Public License as published by the
 *      Free Software Foundation; either version 2 of the License, or (at your
 *      option) any later version.
 *
 ****************************************************************************/
#include <linux/cdev.h>
#include <linux/circ_buf.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/ioctl.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/sched.h>
#include <linux/timer.h>
#include <linux/uaccess.h>
#include <asm/atomic.h>
#include <asm/current.h>

#include <linux/spi/spi.h>
#include <linux/clk.h>

#include "spi_3st.h"
/******************************************************************************
 * init resource
 *****************************************************************************/

/* определим имя соответствующего устройства в dts */
static struct of_device_id spi3st_ids[] = {
    {
        .compatible = "spi3st"
    },
    { /* end of table */ }
};

/* Информируем ядро о том какие устройства мы хотим найти в dts */
MODULE_DEVICE_TABLE(of, spi3st_ids);

/* Заполним структуру драйвера устройства */
static struct platform_driver spi3st_pd = {
    .probe              = spi3st_probe,
    .remove             = spi3st_remove,
    .driver             = 
        {
            .name               = THIS_MODULE_NAME,
            .of_match_table     = spi3st_ids
        }
};

/******************************************************************************
 * Функция выводящая состояния флагов mode
 *
 * @spi   указатель на структуру spi устройства;
 ******************************************************************************/
static void __maybe_unused print_active_mode_bits(
    struct spi_device*  spi
    )
{
    MSG_ERR(
        "Mode bits:\n"
        "\t\tSPI_LSB_FIRST  = %d\n"
        "\t\tSPI_LOOP       = %d\n"
        "\t\tSPI_3WIRE      = %d\n"
        "\t\tSPI_CPHA       = %d\n"
        "\t\tSPI_CPOL       = %d\n"
        "\t\tSPI_CS_HIGH    = %d\n",
        !!(spi->mode & SPI_LSB_FIRST),
        !!(spi->mode & SPI_LOOP),
        !!(spi->mode & SPI_3WIRE),
        !!(spi->mode & SPI_CPHA),
        !!(spi->mode & SPI_CPOL),
        !!(spi->mode & SPI_CS_HIGH)
        );
}

/******************************************************************************
 * Функция начальной инициализации регистров устройства
 *
 * Функция предполагает что память под структуру выделена черех kzalloc
 * поэтому обнуление полей не требуется.
 * 
 * @pdev   указатель на структуру устройства
 *****************************************************************************/
static void spi3st_init_dev_regs(
    struct spi3st_dev*      dev
    )
{
    dev->dev_csr.cr.reg.trans_len  = BITS_PER_BYTE;
    _cr_wr32(dev);

    dev->dev_csr.baud_div = 4;
    _bdr_wr32(dev);
}

/******************************************************************************
 * Реализация метода set_cs - выбор активного уровня линии CS
 *
 * @spi   указатель на структуру spi устройства;
 * @is_hi флаг указывающий на то что активным уровнем CS должна быть "1"
 ******************************************************************************/
static void _spi3st_set_cs(
    struct spi_device*  spi,
    bool                is_hi
    )
{
    struct spi3st_dev* dev = spi_master_get_devdata(spi->master);
    
    dev->dev_csr.cr.mem = _cr_rd32(dev);
    dev->dev_csr.cr.reg.cs_is_pos = is_hi;
    _cr_wr32(dev);
}

/******************************************************************************
 * Реализация метода setup - установка режимов spi устройства
 *
 * В случае некорректного параметра возвращает -EINVAL
 *
 * @spi   указатель на структуру spi устройства
 *
 * Return: 0 - вслучае успешного завершения;
 *         -EINVAL в случае некорректного параметра
 ******************************************************************************/
static int _spi3st_setup(
    struct spi_device*  spi
    )
{
    int _tmp_bauddiv;
    struct spi3st_dev* dev = spi_master_get_devdata(spi->master);
    int ret = -EINVAL;

    dev->dev_csr.cr.mem = _cr_rd32(dev);

    /* проверим и установим сигнал CS */
    if(spi->chip_select > BIT(_SPI3ST_SS_LEN_BITS))
    {
        MSG_ERR("bad CS number\n");
        return(ret);
    }
    dev->dev_csr.cr.reg.ss = BIT(spi->chip_select);

    /* проверм что длина сообщения не больше 2**_SPI3ST_TRANS_WIDTH */
    if(spi->bits_per_word > (BIT(_SPI3ST_TRANS_WIDTH + 1) - 1))
    {
        MSG_ERR("bad word size (in bits)\n");
        return(ret);
    }
    dev->dev_csr.cr.reg.trans_len = spi->bits_per_word;
    dev->bytes_per_word = DIV_ROUND_UP(spi->bits_per_word, BITS_PER_BYTE);

    dev->dev_csr.cr.reg.lsb_mode    = !!(spi->mode & SPI_LSB_FIRST);
    dev->dev_csr.cr.reg.loopback    = !!(spi->mode & SPI_LOOP);
    dev->dev_csr.cr.reg.en_3st      = !!(spi->mode & SPI_3WIRE);

    dev->dev_csr.cr.reg.cpha_rx     = !!(spi->mode & SPI_CPHA);
    dev->dev_csr.cr.reg.cpha_tx     = !!(spi->mode & SPI_CPHA);
    dev->dev_csr.cr.reg.cpol        = !!(spi->mode & SPI_CPOL);

    dev->dev_csr.cr.reg.cs_is_pos   = !!(spi->mode & SPI_CS_HIGH);

    /* устройство не поддерживает параллельную шину */
    if(   spi->mode & SPI_TX_DUAL
       || spi->mode & SPI_TX_QUAD
       || spi->mode & SPI_RX_DUAL
       || spi->mode & SPI_RX_QUAD
      )
    {
        MSG_ERR("device not provided dual or quad bus width\n");
        return(ret);
    }

    _cr_wr32(dev);

    /*
     * деление на 2 нужно из-за того что делитель задает время не пириода, а
     * полупериода
     */
    _tmp_bauddiv = (dev->sys_clk_rate / spi->max_speed_hz) / 2;
    if(_tmp_bauddiv < MIN_SPI_CLK2SCLK_RATE)
    {
        MSG_ERR("sclk frequency too hi\n");
        return(-EINVAL);
    }

    dev->dev_csr.baud_div = _tmp_bauddiv;
    _bdr_wr32(dev);

    return(0);
}

/******************************************************************************
 * Функция запускающая транзакцию обмена
 *
 * При вызове функция копирует из буфера с данными кусок равный размеру слова
 * spi в усторйство и зупускает транзакцию
 * 
 * @pdev    указатель на структуру шины pdev
 *
 * Возвращает 0 в случае успеха, или код ошибки
 *****************************************************************************/
static int spi3st_tx(
    struct spi3st_dev*  dev
    )
{
    int ret = 0;
    if(!dev->transfer_len)
    {
        MSG_ERR("err: transfer len = 0 \n");
        return(-EINVAL);
    }

    /* если указатель на буфер пуст, согласно протоколу будем слать 0 данные */
    if(IS_ERR_OR_NULL(dev->tx_buf))
    {
        _clean_tx_reg(
            dev
            );
    }
    else
    {
        if(_tx_wr32(dev))
        {
            return(ret);
        }
    }

    dev->dev_csr.cr.mem = _cr_rd32(dev);
    dev->dev_csr.cr.reg.ie_en = 1;
    dev->dev_csr.cr.reg.run   = 1;
    _cr_wr32(dev);
    return(0);
}

/******************************************************************************
 * Реализация метода transfer_one - выполнение одиночного трансфера
 *
 * Возвращает количетство переданных байт
 *
 * @master  - указатель на структуру контроллера spi;
 * @spi     - указатель на структуру устройства spi;
 * @t       - указатель на труктуру трансфера
 ******************************************************************************/
static int _spi3st_txrx(
    struct spi_master*      master,
    struct spi_device*      spi,
    struct spi_transfer*    t
    )
{
    struct spi3st_dev* dev = spi_master_get_devdata(master);

    dev->tx_buf         = (void*)t->tx_buf;
    dev->rx_buf         = t->rx_buf;
    dev->transfer_len   = t->len;
    dev->bytes_per_word = DIV_ROUND_UP(t->bits_per_word, BITS_PER_BYTE);

    dev->dev_csr.cr.mem       = _cr_rd32(dev);
    dev->dev_csr.cr.reg.ie_en = 1;
    _cr_wr32(dev);

    spi3st_tx(dev);
    return(t->len);
}

/******************************************************************************
 *  Обработчик прерываний.
 *         
 *  @irq     номер прерывания которое должжно быть обработано;
 *  @dev_id  указатель но данные которые передаются в обработчик;
 *
 *  Return: Взвращает состояние по завершению обработчика (является элементом
 *  перечисления "enum irqreturn".
 *****************************************************************************/
irqreturn_t spi3st_irq_handler(
    int     irq,
    void*   dev_id
    )
{
    struct spi_master*  master = (struct spi_master*)dev_id;
    struct spi3st_dev*  dev    = spi_master_get_devdata(master);
    u32                 rr[SPI_MAX_PAYLOAD_WORD32];
    u32                 copy_size;

    /*
     * прочитаем в промежуточный буфер весь регистр принятых данных, и копируем
     * в приемный буфер требуемое количество байт оттуда (не больше чем размер
     * единичной транзакции SPI)
     */
    _rx_rd32(
        dev,
        rr
        );
    copy_size = min(
        dev->transfer_len,
        dev->bytes_per_word
        );
    if(!IS_ERR_OR_NULL(dev->rx_buf))
    {
        memcpy(
            dev->rx_buf,
            rr,
            copy_size
            );
    }

    /*
     * вычтем размер транзакции из общего количества данных в обрабатываемом
     * трансфере. Это валидно потому, что раз есть прерывание, значит была
     * транзакция, и передано некоторое количество байт. Теперь когда rx буфер
     * заполнен можем уменьшить счетчик данных для обработки
     */
    dev->transfer_len -= copy_size;

    dev->dev_csr.cr.mem = _cr_rd32(dev);
    dev->dev_csr.cr.reg.ie_en = 0;
    _cr_wr32(dev);
    mb();

    /* 
     * если данные для чтения/передачи еще есть - включим прерывание и запустим
     * транзакцию обмена
     */
    if(dev->transfer_len > 0)
    {
        dev->dev_csr.cr.reg.ie_en = 1;
        _cr_wr32(dev);

        spi3st_tx(dev);
    }
    else
    {
        spi_finalize_current_transfer(master);
    }

    return(IRQ_HANDLED);
}

/******************************************************************************
 *  Функция регистрирования обработчиков (top/bottom).
 *         
 *  @dev     указатель на структуру представляющую устройство;
 *  @pdev    указатель на структуру шины platform_device, на которой
 *                  будет зарегистриировано устрйство и драйвер;
 *
 *  Return: Возвращает 0 в случае успеха, код ошибки в противном случае.
 *****************************************************************************/
static int spi3st_register_irq(
    struct spi_master*          master,
    struct platform_device*     pdev
    )
{
    int                 ret_value = 0;
    struct spi3st_dev*  dev = spi_master_get_devdata(master);

    dev->irq = platform_get_irq(
        pdev,
        0
        );
    if (dev->irq < 0)
    {
        MSG_ERR("Bad irq request\n");
        ret_value = dev->irq;
        goto spi3st_register_irq_exit;
    }

    ret_value = devm_request_threaded_irq(
        &pdev->dev,                 /* указатель на структуру device        */
        dev->irq,                   /* номер обрабатываемого прерывания     */
        spi3st_irq_handler,         /* обработчик верхней пололвины         */
        NULL,                       /* обработчик нижней половины           */
        0,                          /* флаги                                */
        pdev->name,                 /* имя модуля                           */
        master                      /* указатель на структуру устройства    */
        );
    if(ret_value)
    {
        MSG_ERR("Bad irq register\n");
    }

spi3st_register_irq_exit:
    return(ret_value);
}

/******************************************************************************
 * Реализация функции probe
 * 
 * @pdev    указатель на структуру шины pdev
 *
 * Return: Возвращает 0 в случае успеха, или код ошибки
 *****************************************************************************/
static int spi3st_probe(
    struct platform_device*         pdev
    )
{
    int                     ret_value = -EBUSY;
    u32                     bus_id_of;
    struct spi3st_dev*      dev;
    struct resource*        res_mem_csr;
    struct clk*             dev_clk;
    struct spi_master*      master;

    master = spi_alloc_master(
        &pdev->dev,
        sizeof(struct spi3st_dev)
        );
    if(!master)
    {
        ret_value = -ENODEV;
        goto spi3st_probe_exit;
    }

    master->bus_num             = pdev->id;
    master->num_chipselect      = _SPI3ST_SS_LEN_BITS;
    master->bits_per_word_mask  = SPI_BPW_RANGE_MASK(1, 32);
    master->dev.of_node         = pdev->dev.of_node;
    master->transfer_one        = _spi3st_txrx;
    master->setup               = _spi3st_setup;
    master->set_cs              = _spi3st_set_cs;
    /* перечислим маски допустимых опций */
    master->mode_bits           =   SPI_CPHA
                                  | SPI_CPOL
                                  | SPI_CS_HIGH
                                  | SPI_LSB_FIRST
                                  | SPI_3WIRE
                                  | SPI_LOOP;

    dev = spi_master_get_devdata(master);

    /* запросим величину тактовой частоты устройства */
    dev_clk = devm_clk_get(
        &pdev->dev,
        NULL
        );
    if(IS_ERR_OR_NULL(dev_clk))
    {
        MSG_ERR("bad clock request for dev object\n");
        goto spi3st_probe_exit_free_master;
    }
    dev->sys_clk_rate = clk_get_rate(dev_clk);

    res_mem_csr = platform_get_resource(
        pdev,
        IORESOURCE_MEM,
        0
        );
    if(!res_mem_csr)
    {
        MSG_ERR("bad io resources request\n");
        ret_value = -EINVAL;
        goto spi3st_probe_exit_free_master;
    }

    /* получения региона памяти с отображением */
    dev->csr_io = devm_ioremap_resource(
        &pdev->dev,
        res_mem_csr
        );
    if(IS_ERR_OR_NULL(dev->csr_io))
    {
        ret_value = PTR_ERR(dev->csr_io);
        goto spi3st_probe_exit_free_master;
    }

    ret_value = of_property_read_u32(
        pdev->dev.of_node,
        "bus-num",
        &bus_id_of
        );
    if (ret_value >= 0)
        master->bus_num = bus_id_of;

    /* проведем начальную инициализацию регистров устройства */
    spi3st_init_dev_regs(
        dev
        );

    /* зарегистрируем обработчики прерываний */
    ret_value = spi3st_register_irq(
        master,
        pdev
        );
    if(ret_value)
    {
        goto spi3st_probe_exit_free_master;
    }

    ret_value = devm_spi_register_master(
        &pdev->dev,
        master
        );
    if(ret_value)
    {
        goto spi3st_probe_exit_free_master;
    }

    /* сохраним указатель на структуру нашего устройства */
    platform_set_drvdata(
        pdev,
        (void*)master
        );

    return(0);

spi3st_probe_exit_free_master:
    spi_master_put(master);

spi3st_probe_exit:
    MSG_ERR("probe of \"spi3st\" is failed :(\n");
    return(ret_value);
}

/******************************************************************************
 * Реализация функции remove
 * 
 * @pdev    указатель на структуру шины pdev
 *
 * Return: Возвращает 0 в случае успеха, или код ошибки
 *****************************************************************************/
static int spi3st_remove(
    struct platform_device*     pdev
    )
{
/*
 *    struct spi_master*  master  = platform_get_drvdata(pdev);
 *
 *    spi_unregister_master(master);
 */

    return 0;
}
/******************************************************************************
 * Функция инициализации ресурсов вызывающаяся при загрузку модуля в ядро
 *
 * Return: возвращает 0 в случае успеха, или код ошибки.
 *****************************************************************************/
static int __init spi3st_init(
    void
    )
{
    int     ret_value = 0;

    // Зарегистрируем нашу платформу в ядре
    ret_value = platform_driver_register(
        &spi3st_pd
        );
    if(ret_value)
    {
        MSG_ERR(
            "platform_driver_register returned %d\n",
            ret_value
            );
    }
    else
    {
        MSG_INFO("module successfully initialized!\n");
    }

    return(ret_value);
}

/******************************************************************************
 *Функция очистки ресурсов при выгрузке модуля
 *****************************************************************************/
static void __exit spi3st_exit(
    void
    )
{
    platform_driver_unregister(&spi3st_pd);
    
    MSG_INFO("spi3st_pd module successfully unregistered\n");
}

/*подсунем ядру функции инициализации/выхода нашего модуля*/
module_init(spi3st_init);
module_exit(spi3st_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Igor Bolshevikov <bolshevikov.igor@gmail.com>");
MODULE_DESCRIPTION("Driver for spi 3st (custom IP core)");
MODULE_VERSION("1.0");
