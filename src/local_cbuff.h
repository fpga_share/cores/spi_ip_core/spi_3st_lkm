/*****************************************************************************
 * Описание:
 *      .
 *
 * Комментарии:
 *
 * Разработчик:
 *      Igor Bolshevikov <bolshevikov.igor@gmail.com>
 *
 * license:
 *      This program is free software; you can redistribute it and/or modify it
 *      under the terms of the GNU General Public License as published by the
 *      Free Software Foundation; either version 2 of the License, or (at your
 *      option) any later version.
 *
 ****************************************************************************/
#ifndef _MMBOX_CBUFF_H_
#define _MMBOX_CBUFF_H_

#include <linux/circ_buf.h>

struct cbuff
{
    /** структура ядреного кругового буфера */
    struct circ_buf     cbuff;
    /** размер (количество элементов) кругового буфера */
    ssize_t             size;
};

/******************************************************************************
 *  Обертки для вышеуказанных маккроопределений, позволяющие работать не с
 *  отдельными полями, а со структурой circ_buf
 ******************************************************************************/

/**
 * Подсчет заполнености буфера
 *
 * @circ указатель на структуру struct circ_buf
 */
#ifdef circ_count
#   error("circ_count is defined earlier, it has unsuitable parameters")
#endif
#define circ_count(circ) \
    (CIRC_CNT((circ)->cbuff.head, (circ)->cbuff.tail, (circ)->size))

/**
 * Подсчет заполнености буфера без заварота через конец буфера
 *
 * @circ указатель на структуру struct circ_buf
 */
#ifdef circ_count_to_end
#   error("circ_count_to_end is defined earlier, it has unsuitable parameters")
#endif
#define circ_count_to_end(circ) \
    (CIRC_CNT_TO_END((circ)->cbuff.head, (circ)->cbuff.tail, (circ)->size))

/**
 * Подсчет свободного места в буфере
 *
 * @circ указатель на структуру struct circ_buf
 */
#ifdef circ_space
#   error("circ_space is defined earlier, it has unsuitable parameters")
#endif
#define circ_space(circ) \
    (CIRC_SPACE((circ)->cbuff.head, (circ)->cbuff.tail, (circ)->size))

/**
 * Подсчет свободного места в буфере без заворота через конец буфера
 *
 * @circ указатель на структуру struct circ_buf
 */
#ifdef circ_space_to_end
#   error("circ_space_to_end is defined earlier, it has unsuitable parameters")
#endif
#define circ_space_to_end(circ) \
    (CIRC_SPACE_TO_END((circ)->cbuff.head, (circ)->cbuff.tail, (circ)->size))


/**
 * Инкремент указателя head в кольцевом буфере на num слов
 *
 * Конструкция smp_store_release требуется для уверенности что предыдущие
 * операции доступа к памяти завершены, иначе можем получить переупорядочивание,
 * и в результате инкремент указателя head перед записью в память, а не после
 *
 * @circ_buf  указатель на структуру кольцевого буфера;
 * @num       число слов которое нужно прибавить
 */
#define circ_add_to_head(circ, num)             \
    ({smp_store_release(                        \
          &(circ)->cbuff.head,                  \
            ((circ)->cbuff.head + num)          \
          & ((circ)->size - 1)                  \
          );                                    \
    })

/**
 * Инкремент указателя tail в кольцевом буфере на num слов
 *
 * Конструкция smp_store_release требуется для уверенности что предыдущие
 * операции доступа к памяти завершены, иначе можем получить переупорядочивание,
 * и в результате инкремент указателя tail перед чтением из памяти, а не после
 *
 * @circ_buf  указатель на структуру кольцевого буфера;
 * @num       число слов которое нужно прибавить
 */
#define circ_add_to_tail(circ, num)             \
    ({smp_store_release(                        \
        &(circ)->cbuff.tail,                    \
          ((circ)->cbuff.tail + num)            \
        & ((circ)->size - 1)                    \
          );                                    \
    })


#endif /* _MMBOX_CBUFF_H_ */
